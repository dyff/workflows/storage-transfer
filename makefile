VENV ?= venv
PYTHON ?= $(VENV)/bin/python3
DOCKER ?= docker
GID ?= $(shell id -g)
UID ?= $(shell id -u)


BASE_DIR = $(shell pwd)
PYTHONPATH = $(BASE_DIR)

$(VENV):
	uv venv $(VENV)

.PHONY: compile
compile:
	uv pip compile --python-version 3.9 --upgrade -o requirements.txt requirements.in

requirements.txt: requirements.in | $(VENV)
	uv pip compile --python-version 3.9 --upgrade -o requirements.txt requirements.in

.PHONY: docker-build
docker-build:
	$(DOCKER) build -t "dyff/workflows/storage-transfer" -f Dockerfile .
